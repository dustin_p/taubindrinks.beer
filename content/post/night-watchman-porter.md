---
title: Mangrove Jack’s Night Watchman Porter
author: Dustin Plunkett
type: post
date: 2013-08-08T22:52:26+00:00
categories:
  - Homebrewing
tags:
  - Mangrove Jacks
  - Night watchman
---

I started a new beer about two weeks ago, the <a title="Mangrove Jack's Night Watchman Porter" href="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/08/night-watchman-brewery-box" target="_blank">Mangrove Jack's Night Watchman Porter</a>! I was really excited to try this new beer kit, as it seems to be a step or two above the one I had used last time when I brewed <a title="Taubin…. Brews?" href="https://taubindrinks.beer/taubin-brews/" target="_blank">Cannon Fodder</a>. Unfortunately, I didn't take as many photos this time, as I did with my last beer.

One of the reasons for the lack of photos, is the process was pretty much the same as last time, with a few small differences. The biggest of which, is this one came with hop pellets. After cleaning out the carboy and soaking it in oxyclean overnight, I boiled up the water, added the syrup and tossed it all in the now cleaned carboy. I then added the DME (Dry Malt Extract) with one difference. This time I used a specialty DME blend called Brewblend 15. The main difference is this has more fermentable sugars, and is not as sweet as straight DME.

<!--more-->

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/08/Cleaning.jpg"><img class=" wp-image-412  " title="Cleaning the carboy" alt="Cleaning the carboy" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/08/Cleaning.jpg" width="410" height="614" /></a> Cleaning the carboy

Once everything was cleaned, and added and stirred, I added the blow off tube, and let it sit. Now, let me pause for a moment here, and reflect a bit on my first beer. I was (and am) very proud of my first beer, and showed it off right after I finished putting everything together. One of the places I showed off to was the Homebrew Chatter forums, and boy am I glad I did. After showing it to them, one of them mentioned I should use a <a title="Blowoff Addition" href="http://taubindrinks.com/blowoff-addition/" target="_blank">blow off instead of an airlock</a>, at least for the first few days. Boy am I glad I got that advice, because I woke up the next morning to this:

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/08/PorterGR-2.jpg"><img class=" wp-image-413   " title="Porter Blowoff" alt="Porter Blowoff" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/08/PorterGR-2.jpg" width="553" height="369" /></a> Porter Blowoff

That was some super happy yeast! I'm not sure if it was due to the different DME, or something else, but man this beer was happy! If this had been an airlock instead of a blow off, it would have gotten clogged up, and probably made a rather large mess on the walls, counter, and floor, thus ending my ability to do homebrew for fear that my wife would kill me.

After a few days, everything calmed down, and I switched over from the blow off to the airlock, and haven't had a problem since. As I write this, it's been happily fermenting away for 14 days. I've taken two gravity readings, and it appears as though it's still happily fermenting away, not quite ready to bottle yet. Possibly due to the cooler temperatures we have had here in New Zealand (we are just heading into spring time).

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/08/PorterGR.jpg"><img class=" wp-image-414   " alt="Mangrove Jack's Night Watchman Porter gravity reading" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/08/PorterGR.jpg" width="553" height="369" /></a> Porter Gravity reading - still a bit high

<span style="line-height: 1.714285714; font-size: 1rem;">I will say this though, it is very tasty already!  I took a taste after the gravity reading, and it was great. The coffee notes really come out in this, in fact, I think my wife might even like it! I think next time I do one of these, and trust me, there will be a next time, I'll add a bit of vanilla to it at the same time I add the hops. I think it would give it a nice additional flavor. But for now, it continues to happily bubble away on my counter, getting closer and closer to the time to bottle it. I will definitely keep you all up to date once it's complete, and even do a video review. </span>

Until then, cheers!

