---
title: Craft beer pretentiousness
author: Dustin Plunkett
type: post
date: 2014-05-13T22:09:43+00:00
categories:
  - Beer
  - Rant
tags:
  - Beer Snob
  - Blog
  - Craft beer
  - Craft Beer Snob
  - Pretentious Craft Beer
  - Pretentiousness
  - Rant
---

I was recently reading a <a title="Poor guy" href="http://www.reddit.com/r/beer/comments/25eq66/an_uninformed_enthusiasts_beer_tasting/" target="_blank">Reddit post</a> on <a title="Beerit subreddit" href="http://www.reddit.com/r/beer/" target="_blank">/r/beer</a>, by a person who is new to craft beer, and the amount of craft beer pretentiousness astounded me. The user in the post took time out of his day, to post <a title="Some decent beers" href="https://imgur.com/a/B7NKz#0" target="_blank">30 images</a> of a recent tasting he had with some buddies, and got ripped apart by some people on the community. The thing is, this isn't just in the Reddit community either. There is a consistent pretentiousness that perpetrates throughout the entire community. Whether you read posts on <a title="Beer Advocate" href="http://www.beeradvocate.com/community/" target="_blank">Beer Advocate</a>, <a title="Reddit" href="http://www.reddit.com/" target="_blank">Reddit</a> or elsewhere, there is always a level of snobbery that really needs to stop, as it is pushing out others from our great community.

<!--more-->

The constant bashing of someone who doesn't have a "whale" or dissing of a beer that someone likes that you don't, is not what it should be about. We should be working as a whole, to bring more people into the community, and educating them, instead of constantly bashing them. The craft beer community, should not be about elitists, and getting that super rare beer to stash away in your cellar for 30 years, solely to brag that you have it. I've had a few beers that are super rare, and I was extremely excited to have the opportunity to try them.

Every somewhat rare beer I've had the pleasure of purchasing myself, I've made a point to share. If someone didn't like the beer I was sharing, I made it clear to them, I would rather have them pour it out and let me try to find a beer they would like, instead of suffering through it. I'm a firm believer that there is a beer for everyone. Whether it be a Lambic, Hefeweizen, Pale Ale, IPA, Sour or something else entirely, there simply is a beer for everyone.  Being a pretentious asshat, does nothing but make you look like a pretentious asshat.

It's time to stop the craft beer pretentiousness, and be more welcoming to those that are new, or not so  new to craft beer as a whole. If someone is drinking a crappy Bud Light, instead of bashing them right away, try showing them what good beer is. Take the time to educate them, instead of bashing them. It's time to stop bashing the newbie that brought a Blue Moon to a tasting thinking they are doing the right thing.  And most importantly, stop bashing the people who take the time to take photos, write down notes and share what they thought with people.  Not everyone has the resources to buy massive amounts of super rare beer, and that's okay. At least they are making an effort, and should be applauded for that.

So please, I'm begging the craft beer community as a whole, stop the bashing. Teach others what good beer is. We were all in their shoes once. Now more than ever before, craft beer can be intimidating to get into, and they don't need more barriers keeping them from enjoying what we all enjoy. The craft beer community is supposed to be open and friendly, not a secret society where we bash those that are seen as lesser than us. So please, stop being an asshat, and start being more welcoming.

