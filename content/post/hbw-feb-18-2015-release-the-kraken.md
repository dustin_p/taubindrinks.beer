---
title: HBW Feb 18 2015 – Release the Kraken
author: Dustin Plunkett
type: post
date: 2015-02-18T19:48:54+00:00
categories:
  - Beer
  - Blog
  - HBW
  - Homebrewing
tags:
  - Blog
---
In this episode of HBW (Home Brew Wednesday) I release the kraken with my latest beer, a 9% black IPA. I also discuss a few new brewing tools for filtering and transferring beer, as well as some upcoming tools that I'd like to add to my brewing equipment. I'll be doing HBW episodes either bi-weekly or monthly from here on out. Until next time, drink good beer.

<!--more-->

{{< youtube DvIm-Chli68 >}}

