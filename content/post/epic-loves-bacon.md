---
title: Taubin Drinks Tuesday – Epic Loves Bacon
author: Dustin Plunkett
type: post
date: 2014-10-21T02:25:07+00:00
categories:
  - Blog
tags:
  - Beer
  - Blog
  - Craft beer
  - Epic Brewing
  - Epic Loves Bacon
  - Taubin Drinks Tuesday
---
Today for Taubin Drinks Tuesday, I tried out Epic Loves Bacon by Epic Brewing Company here in Auckland, NZ
    
Epic Loves Bacon is a 6.2% ABV Smoked IPA
    
<!--more-->
    
{{< youtube Otrhy0GxLMM >}}
    
&nbsp;
    
It is not something I would drink every day. In fact, it was fine as a one off, however, I'm not sure I'd actively seek it out. Like most things bacon flavored, it was okay as a novelty, however not something I'd actively seek out all the time. Overall, Epic Loves Bacon wasn't bad, but also not great. Would I drink it again? Well, maybe. If it was offered for free, sure, would I buy it again? No.
    
To me, it was more ham and less bacon as I know bacon. Interesting, but not interesting enough to be a huge deal. Overall, I'd say try it if you find it, but don't actively seek it out.
    
Until next time, drink good beer. Cheers.

