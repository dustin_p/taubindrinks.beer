---
title: Brewing my Brewferm Grand Cru
author: Dustin Plunkett
type: post
date: 2014-05-02T00:08:46+00:00
categories:
  - Beer
  - Blog
  - Homebrewing
tags:
  - Beer
  - Blog
  - Brewferm
  - brewing
  - Craft beer
  - Grand Cru
---

Yesterday I took a shot at brewing the Grand Cru I purchased from Brewferm. Today, I show you how it went.  For those not familiar, Grand Cru is a Belgian Strong Ale.  Belgian Strong Ales are generally light to deep gold in color, with a spicy scent, and a sweet/spicy flavor. They are called strong, because of their alcohol content. Most end at around 6.5-7% ABV. Strong indeed if you are not prepared for it. We'll see how it ends up after it's fully fermented and conditioned, which will be in a couple of months. On with the brewing.  I gathered everything I would need up, and took stock of the situation, as well as gave everything a good rinse out prior to properly cleaning.

<!--more-->

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/05/brewing_1_5_14.jpg"><img class="size-full wp-image-553" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/05/brewing_1_5_14.jpg" alt="Taking Stock" width="1024" height="683" /></a> Taking stock of what's on hand

The first step in any brewing process, is to clean and sanitize. This time around, I switched up the sanitizer, and decided to go with <a title="Iodophor Sanitiser " href="/images/2014/05/1371-iodophor-sanitiser.html">Iodophor Sanitiser</a>. There are a couple of reasons for this, but mostly due to trying something new. I gave everything a good clean out, using some cleaning solution, and moved on to sanitizing everything.

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/05/brewing_1_5_14-2.jpg"><img class="wp-image-554 size-full" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/05/brewing_1_5_14-2.jpg" alt="sanitize" width="1024" height="683" /></a> Sanitize all the things!

The Iodophor has a slight brown tinge to it when put together with water in the proper solution. This was swished around everything, as well as mixed in a spray bottle for spraying things down and left for 2 minutes, then drained, and rinsed per the instructions.  I then left everything to dry.

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/05/brewing_1_5_14-3.jpg"><img class="size-full wp-image-555" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/05/brewing_1_5_14-3.jpg" alt="Drying" width="1024" height="683" /></a> Dry all the things!

While all of that was drying in a clean sterilized place, I boiled up 6 liters of water (one more liter than the recipe called for due to evaporation and all that) in a large pot on the stove.

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/05/brewing_1_5_14-5.jpg"><img class="size-full wp-image-557" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/05/brewing_1_5_14-5.jpg" alt="Boiling water (exciting!)" width="1024" height="683" /></a> Boil all the things!

While that came up to a boil, I took the label off the Brewferm can, and placed in the sink in hot water for 10 minutes. This allows the contents to flow smoothly out of the can and into the fermentation bucket. By that time, the water on the stove had been boiling for a few minutes, and it was time to cool it off using the sink.

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/05/brewing_1_5_14-6.jpg"><img class="size-full wp-image-548" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/05/brewing_1_5_14-6.jpg" alt="Full boil of the water" width="1024" height="683" /></a> Full boil of the water

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/05/brewing_1_5_14-7.jpg"><img class="size-full wp-image-549" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/05/brewing_1_5_14-7.jpg" alt="Cooling the water" width="1024" height="683" /></a> Cooling the boiled water with cold water

As that was cooling, I put the contents of the Grand Cru can into the fermentation bucket, along with three liters of water.  I used warm water to rinse the can adding the first liter of water directly from that, and two more of the boiled water along with that. I then added 500 grams of DME, and gave it a really good stir to mix things up. Once everything had cooled to 20 degrees C (68 for you American folk), I added the dry yeast that was included to warm water and let it sit for 10 minutes to wake the yeast up, and prep it for doing its job of converting all of the sugars to alcohol.

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/05/brewing_1_5_14-8.jpg"><img class="size-full wp-image-550" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/05/brewing_1_5_14-8.jpg" alt="yeast" width="1024" height="683" /></a> Yeast in water to wake it up

While the yeast was resting, I added the rest of the water to the fermentation bucket, and finally added the yeast, giving everything a gentle swirl before sealing it up, and affixing a blow off tube to safely release excess gasses. This is a smaller batch than my previous two, at only 9 liters, but it should be quite good.

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/05/brewing_1_5_14-9.jpg"><img class="size-full wp-image-551" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/05/brewing_1_5_14-9.jpg" alt="Grand Cru ready to go" width="1024" height="683" /></a> Grand Cru ready to do its thing

Finally, I covered my so far nameless Grand Cru with a shirt to keep any extra light out, and moved it to the garage where it will happily sit to ferment for at least the next 10 days. After it has settled according to a hydrometer, I will rack it to secondary for a further 1-2 weeks and finally bottle it, giving it a cool name, and letting it sit for another few weeks before enjoying it.

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/05/brewing_1_5_14-10.jpg"><img class="size-full wp-image-552" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/05/brewing_1_5_14-10.jpg" alt="Grand Cru in the fermenter" width="1024" height="683" /></a> Grand Cru in the fermenter doing its thing

I'll update again when I transfer to secondary!

