---
title: Batch One taste and an update on my next beer
author: Dustin Plunkett
type: post
date: 2014-10-14T04:16:11+00:00
categories:
  - Beer
  - Blog
  - Homebrewing
tags:
  - Beer
  - Blog
  - Brewferm
  - brewing
  - Craft beer
  - Drunken Pukeko
  - Grainfather
  - Homebrew
  - New Zealand Beer
---

I had a taste of Batch One, and let me tell you, it was great!. I also have an update of the next upcoming beer, which I brewed this past weekend.
    
Batch one carbonated great in the keg. Even with my previous mistakes, it turned out quite amazing. I was pleasantly surprised with how Batch One has turned out. The flavor and carbonation is great, and it is a very drinkable beer. It is honestly my best beer to date. You can see a video of the pour and taste below.
    
<!--more-->
    
{{< youtube -KL2S1ZsUoI >}}
    
I will have a full review of Batch One up as soon as I have a chance to film it, as well as a review of Epic Loves Bacon by Epic Brewing. I will hopefully have those up this week, and bring the blog back to beer tastings as well as homebrewing.
    
Now on to my next beer. My friend Bruce has been very supportive of my brewing ventures, and has encouraged me along the way. He also prefers dark beer to lighter ones, so I decided my second all grain beer would be a dark beer for him. I decided to pick up a Brewferm all grain kit of "Black Gold" which they describe as a "stout style" beer. I had a a chance to brew the beer this last weekend on my Grainfather, and it went extremely well.
    
The beer poured into the fermenter black as night, and tasted very good from my tastings in the hydrometer. I can't wait to try it out. The fermentation was crazy in the first 24 hours, in fact it clogged my airlock overnight the first night. Since then it's calmed down and cooled down a bit. You can see video of the beer below.
    
{{< youtube Xa7PQpsOgfQ >}}
    
I can't wait to get this one in the kegs, and get it cooled down. It should be amazing. I have yet to name it, but once it does have a name I'll be sure to let everyone know. I'll avoid calling it Batch Two I promise.
    
That's about it for this update, I'll hopefully have the next Taubin Drinks episode done this week, until then, drink good beer. Cheers!

