---
title: First brew – All grain brewing
author: Dustin Plunkett
type: post
date: 2014-09-18T05:00:54+00:00
categories:
  - Blog
  - Homebrewing
tags:
  - Blog
  - brewing
  - Grainfather
---
I've just completed my first brew using an all grain brewing system. It was a freaking blast! It was a lot of hard work, but I know it will taste better than any other beer I've made yet. Moving to an all grain brewing system will allow me to control every part of the brew day. The water, grain, hops, everything. There will be video added once it's edited for now let's get on with how things went:

After weeks of waiting, the package finally arrived. Unfortunately, the asshats at the courier service decided to not knock on my door at all, and left a note instead of dropping it off. No big deal, I grabbed my ticket, and headed up to the couriers office to pick it up. To my surprise, there were two boxes. One contained the Grainfather itself and the other contained an all grain kit for what would become my first beer. A Chur! NZ Pale Ale clone.

<!--more-->

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/09/box.jpg"><img class="wp-image-814" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/09/box-682x1024.jpg" alt="box" width="442" height="664" /></a> Boxes!

&nbsp;

I ripped the box open and carefully laid everything out, taking note to make sure everything was there. Luckily for me, it was. Every piece and valve was accounted for.

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/09/the_kit.jpg"><img class="wp-image-806 size-large" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/09/the_kit-1024x682.jpg" alt="the_kit" width="474" height="315" /></a> Grainfather laid out

&nbsp;

Once everything was laid out, I realized how much goes into an all grain brewing system like this. It was very simple to figure out though. The instructions that came with it were very clear. I then turned my attention to the all grain kit it came with. It is a clone of Chur! by Behemoth Brewing in New Zealand. It's a beer I've had many times and very much enjoy. I was extremely excited to see it included.

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/09/chur.jpg"><img class="wp-image-815 size-large" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/09/chur-1024x682.jpg" alt="chur" width="474" height="315" /></a> Chur! Pale Ale ingredients

I followed the instructions carefully and put the system together. I then ran it through a cleaning cycle using the cleaning solution included. This involved bringing the temp up to 55° C (131° F), putting in one oz of the cleaning solution, and running it through the counterflow for 5 minutes, then through the arm for 15 minutes. This allowed all of the oils and chemicals from the manufacturing process to be cleaned from the system.

Once that was completed, I drained and rinsed the system entirely. I then added the water to the system, and raised the temps up to mashing temp.

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/09/heat_water.jpg"><img class="wp-image-802 size-large" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/09/heat_water-690x1024.jpg" alt="heat_water" width="474" height="703" /></a> Heating the water

Once the water reached 67° C (153° F), it was time to add the grains. I capped the overflow pipe, and slowly poured in the grains. Once they were added, I placed the top mesh plate in place, as well as the overflow, and mashing arm, then turned on the pump and let it mash for 75 minutes at temp to pull the sweet sugars out of the grains.

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/09/added_grain.jpg"><img class="wp-image-810 size-large" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/09/added_grain-752x1024.jpg" alt="added_grain" width="474" height="645" /></a> Mashing the grain

Once the 75 minutes was up, I mashed out at 75° C (176° F) for 10 minutes

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/09/end_of_mash.jpg"><img class="wp-image-800 size-large" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/09/end_of_mash-1024x682.jpg" alt="end_of_mash" width="474" height="315" /></a> End of mash - 75 minutes later and much clearer

Once the mash out was completed, it was time to sparge the grains. To do this, I removed the arm assembly, capped the pump arm and attached the lifting arm to the grain basket. I then simply lifted the grain basket out, and turned it 90° to lock it on top of the kettle. It was then a matter of pouring 12.5 L (3.3 US Gallons) of heated water over the grain, to wash out as much of the sugar as possible into the boiler.

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/09/ready_for_sparge.jpg"><img class="wp-image-803 size-large" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/09/ready_for_sparge-682x1024.jpg" alt="ready_for_sparge" width="474" height="711" /></a> Ready to sparge. Quite convenient to have the basket rest on top of the kettle

Once the grain was sparged, I removed the entire thing, and placed it in my sink. I had a brief moment of freaking out when I realized I hadn't thought of where to place it, but the sink worked out great with no mess. This also allowed the spent grain to cool enough during the boil to be handled for disposal (I put it out for the birds, but so far, they haven't seemed interested).

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/09/spent_grain.jpg"><img class="wp-image-805 size-large" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/09/spent_grain-1024x682.jpg" alt="spent_grain" width="474" height="315" /></a> Spent grain

It was now time to boil! This is the really fun part of the brew day. This is when the hops get added to the wort. As well as finings like Irish Moss. The ramp up time on the Grainfather was really fast, getting it from 75 to boil. Once the water started to boil, I started a 90 minute timer.

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/09/up_to_boil.jpg"><img class="wp-image-809 size-large" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/09/up_to_boil-1024x682.jpg" alt="up_to_boil" width="474" height="315" /></a> Up to a boil

At 60 minutes, I added my first hop addition. I've heard many times in the past, that there is a chance for boil over especially during additions, and learned this first hand. Luckily, I was prepared. I had my trusty stir paddle, as well as a spray bottle of water just in case. Luckily, it didn't boil over. It came closer than I'd like, but I was able to stir it back down without a problem.  I then had a 10 minute hop addition with Irish Moss, and a final hop addition at flame out.

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/09/flameout.jpg"><img class="wp-image-801 size-large" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/09/flameout-1024x682.jpg" alt="flameout" width="474" height="315" /></a> Flameout You can see the hop addition disolving in the wort

After the flame out, I let the wort cool for 15 minutes. Then it came time to sanitize the counterflow wort chiller. This was incredibly easy, as the wort was still hot enough to kill any nasties. So I connected it up, and ran the wort through it back into the kettle for 5 minutes.

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/09/sanatize_wort_chiller.jpg"><img class="wp-image-804 size-large" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/09/sanatize_wort_chiller-1024x682.jpg" alt="sanatize_wort_chiller" width="474" height="315" /></a> Wort chiller sanatization

Finally, it was time to turn on the sink faucet and cool the wort down. There are four tubes on the wort chiller. There's a wort input, wort output, cold input and cold output. The cold water flows through a tube outside of the wort tubing, in the opposite direction the wort flows. This cools the wort incredibly fast. I connected the wort outflow to my sanitized fermentation bucket, and let it flow. Then I added the yeast and gave everything a really good shake to get the wort oxiginated.

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/09/transfer.jpg"><img class="wp-image-808 size-large" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/09/transfer-1024x682.jpg" alt="transfer" width="474" height="315" /></a> Wort transfer to the fermenter

Now completed, the waiting game begins. The beer will now sit in the fermentation bucket for a couple of weeks, until the yeast has finished doing it's thing. Then it will be transferred to bottles and let sit for a while longer.

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/09/complete.jpg"><img class="wp-image-799 size-large" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/09/complete-751x1024.jpg" alt="complete" width="474" height="646" /></a> Beer completed

I can say this is the most rewarding brewing experience I've had yet, and I very much look forward to being able to try my first all grain beer. I will definitely update once I've been able to try it out.

I also took video of the entire brewing process, which will be up as soon as possible, so look forward to that as well! Until then, drink good beer!

