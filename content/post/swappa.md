---
title: Swappa?
author: Dustin Plunkett
type: post
date: 2013-04-20T05:04:36+00:00
categories:
  - Blog
  - Homebrewing
tags:
  - brewing
---

Swappa, the great Kiwi custom of getting a 'swappa crate' of beer, drinking all of it, and then taking the empties back in exchange for fulls (after paying some cash of course). It's a bit like the bottle deposits where I used to live. You see, growing up, if you bought a beer, or a soda, you would pay an extra 10 cents per bottle. Then once you were done drinking the contents, you would return those bottles for 10 cents each. It's a great system to get people to recycle. And amazing when you were low on funds and needed a couple of bucks quick.

<!--more-->

Now, why is this so great for me you ask? Because I was able to find a liquor shop that was willing to sell me 3 swappa crates of empties for $5 each! Why is that so great? Because it's a heck of a lot cheaper than buying new bottles. Apparently a lot of people here use Swappa bottles. They are 745ml, and cheap. What more could you want?

&nbsp;

<a title="Swappa by taubinphoto, on Flickr" href="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/04/8663799309"><img title="The great Kiwi Swappa Crate" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/04/_d_improd_/8663799309_7ce1ea6feb_f_improf_500x333.jpg" alt="Swappa" width="500" height="333" /></a> Behold! The Kiwi Swappa Crate!

<span style="font-size: 1rem; line-height: 1.714285714;">So, what do you do with the swappa? Well, they can be pretty filthy, after all, they were filled with really cheap beer. The kind of stuff I would only drink if it was free. So, once you have your creates home, you must clean them. And I mean CLEAN THEM. They were pretty filthy, full of nasties, but, not too bad. I've read stories about gum, </span>cigarette<span style="font-size: 1rem; line-height: 1.714285714;"> butts, and all kinds of other nasty things in them. This batch, only a bit of mould. </span>

So, currently, they are sitting in a tub of REALLY hot water and bleach. Once they are finished with their bleach bath, they'll be drained, dried, and bleached again. At that point, they'll be cleaned with bottle cleaner, and finally (after being inspected for defects), they'll be sanitised and filled on bottle day.

<a title="Bleaching by taubinphoto, on Flickr" href="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/04/8663843393"><img title="Bleach all the things!" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/04/_d_improd_/8663843393_0f5f532b2f_f_improf_500x333.jpg" alt="Bleaching" width="500" height="333" /></a> Bleach all the things!

Once these are done, there's only two more crates to go! They'll go through the same process that these are, and at $5/each you can't beat the price! Even my wife likes the crates they came in.

Other than that, things are bubbling away steadily with the fermentation process. I did pick up a bucket to use as a bottling bucket. That way, I can use an autosiphon to take the beer from the fermenter, into the bottle bucket with a spigot, and bottle that way. It makes things a lot cleaner and easier. I just need to pick up a few more parts for it, like the spigot.

<a title="Mah Bucket by taubinphoto, on Flickr" href="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/04/8663462833"><img title="Mah Bucket!" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/04/_d_improd_/8663462833_6fe028f66a_f_improf_333x500.jpg" alt="Mah Bucket" width="333" height="500" /></a> Mah Bucket!

<span style="line-height: 1.714285714; font-size: 1rem;">So, that's where I sit now. No major updates. A few more days or so until it's bottling day. The fermenting has slowed down a bit, so I'll have to take a gravity reading soon, wait 2 days and take another to see how it's going. I'll definitely be updating at that point. Cheers!</span>


