---
title: Quick update
author: Dustin Plunkett
type: post
date: 2015-01-21T21:07:49+00:00
categories:
  - Blog
tags:
---
Just a quick update. It's been a while since I've posted, and I apologize for that. My wife and I went out of town for the holiday season, and are just getting back into the swing of things, both with our personal lives, and with work items. Work has been a bit hectic lately, which has cut down on my time for brewing and making videos.
    
&nbsp;
    
That being said, I did have time to brew a Kraken kit yesterday. It's a nice hoppy black IPA. It's currently sitting in my kegerator to ferment at a nice controlled temperature. The beer has a nice combination of Pacific Jade, Simcoe, Mosaic, and Citra. It should be excellent.
    
&nbsp;
    
As for what's to come, well, I hope to have a video up sooner rather than later. Either a tasting, or a HBW episode. As for brewing, I want to find a good Vanilla Java Porter recipe and make that my next beer. If anyone has any recommendations for a recipe, please let me know. I promise I will update more as things calm down a bit and we get back into a routine. Until next time, drink good beer!

