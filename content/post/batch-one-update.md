---
title: Batch One update
author: Dustin Plunkett
type: post
date: 2014-09-30T06:55:32+00:00
categories:
  - Blog
  - Homebrewing
tags:
  - Batch One
  - Beer
  - Blog
  - brewing
  - Homebrew
---


I have a few things to talk about, but first off, is the Batch One update. I took the first hydrometer reading today, 12 days into the fermentation cycle, and it's right where it needs to be.  <a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/09/First-Gravity-Background.jpg">
</a>

The reading came  in at 1.012, which is spot on. Now we just wait a few more days, and take a second reading to make sure it is ready for bottling. The last thing you want, is to bottle beers that aren't finished fermenting. When you do that, they have a chance to over-pressurize and explode, which is not something anyone wants.

<!--more-->

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/09/IMG_20140930_1651461.jpg"><img class="wp-image-841 size-full" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/09/IMG_20140930_1651461.jpg" alt="IMG_20140930_165146[1]" width="1024" height="768" /></a> 1.012 Spot on!Another update that some of the more observant of you may have noticed, is a url change. The site has changed from taubindrinks.com to taubindrinks.beer which is more fitting, as that is what this site is about.


I'll hopefully be updating more and more, as I brew more and more, as well as try new beers, as well as post updates about my upcoming beers and their progress.  You can continue to look forward to my successes and failures.

I took a brief video of the gravity reading. In the past, I was stupidly adding the beer to the hydrometer tube prior to putting the hydrometer in. This time, I had a flash of brilliance and put the hydrometer in the tube first. It worked out MUCH better that way.
<iframe src="//www.youtube.com/embed/_2oqU1uG4bo" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

&nbsp;

Other than that, not a whole lot of changes, other than I will be kegging my first beer! That's right, this Friday I'm picking up a kegerator and a couple of brand spanking new kegs from my favorite homebrew shop. I'll be sure to video tape that process, so everyone can see how that goes as well.

Oh, and I have set up brewpi to keep an eye on things while they ferment! It should be quite fun! Until then, drink good beer!

