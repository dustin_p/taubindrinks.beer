---
title: HBW March 11 2015
author: Dustin Plunkett
type: post
date: 2015-03-12T07:47:44+00:00
categories:
  - Beer
  - Blog
  - HBW
tags:
  - Blog
  - HBW
  - Homebrew
  - Homebrew Wednesday
---

In this weeks HBW, I discuss beer recipes, Bell's suing another brewery, as well as future brewing and HBW plans. I apologise for last weeks episode, I wasn't really feeling it when I made it, which I think shows a bit.

<!--more-->
    
{{< youtube UUuS9DMtWNs >}}

[Bell's Sues Innovation Brewing](http://www.freep.com/story/money/business/michigan/2015/03/10/bells-brewery-beer-trademark-innovation/24732355/)

[Brewing Classic Styles](http://www.whitcoulls.co.nz/brewing-classic-styles-80-winning-recipes-anyone-can-brew-978093738192237908)
    
My next HBW episode will not be until my next brew day, which should be in a couple of weeks. Until then, drink good beer! Cheers!


