---
title: Crafty beer vs craft beer who are you drinking?
author: Dustin Plunkett
type: post
date: 2013-04-18T02:39:17+00:00
categories:
  - Beer
---

I love craft beer. I love the innovation, the taste, the feel, the passion of craft beer. I don't want to be told what to drink. I don't want to have a bunch of ads shoved in my face showing me how great a particular beer is. I want to be able to look a brewer, or bartender in the face at their brewery, and say "Ya know, I don't really care for this particular beer" and have that be OKAY, and have them accept that, and know that it's okay for not everyone to love their beer.

<!--more-->

<a title="Lukcy Basartd - 2012 by taubinphoto, on Flickr" href="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/04/8236729628"><img class="aligncenter" alt="Lukcy Basartd - 2012" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/04/8236729628_f98f8d9be5.jpg" width="302" height="500" /></a>

&nbsp;

The first time I went to <a title="Stone Brewing" href="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/04/www.stonebrew.com" target="_blank">Stone Brewing Co.</a> in San Diego, I sent a Stone Ruination IPA back. And you know what, it was fine. The waitress understood and knew that not everyone likes such hoppy beers. Well, that's been a few years now, and I honestly love Ruination now, but, that's another blog post all together. You see, the craft beer industry in America, and abroad, has grown tremendously in the past 20 years. In fact, according to the <a title="Brewers Association Statistics 2012-13" href="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/04/facts" target="_blank">Brewers Association</a>, 409 brewery openings, consisting of 310 microbreweries and 99 brewpubs opened in 2012 alone. And do you know who this scares more than anything? Big brewers. The brewers that are in it for one thing, and that's the cash.

Big brewers don't care about their consumer. They only care that they HAVE consumers. They spend billions of dollars every year on advertising to constantly shove their product in your face, because they NEED to do that. In 2011, according to <a title="Business Insider" href="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/04/the-35-companies-that-spent-1-billion-on-ads-in-2011-2012-11?op=1" target="_blank">Business Insider</a>, AB-Inbev (better known as Anheuser-Busch or Budweiser) spent a whopping $1.42 billion in advertising. They did this because they know if they don't constantly shove their product in your face, you'll stop buying it because it's bland, tasteless and boring.

It doesn't stop at advertising though, oh no... It goes far beyond that. AB-Inbev and SAB Miller buy and create 'Crafty Breweries' in order to trick you into drinking them and thinking it's someone else.  These two companies alone, own more than 200 brands. Now, let that sink in for a moment. Two companies, over 200 brands. That's 200 different 'breweries' that these two own. AB-Inbev owns a lot of the big hitters, brands  you know and may like! Brands you probably don't realize are owned by a huge company.

[caption id="attachment_257"width="255"]<img class="size-full wp-image-257" alt="Corona" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/04/corona.jpg" width="255" height="300" /> Corona

Brands like:

Corona
Becks
Stella Artois
Leffe
Hoegaarden
Spaten
Bass
Rolling Rock
Shock Top

The list goes on and on and on.

Every time you open one of these beers, or the rest that are owned by AB-Inbev/SAB-Miller, you are putting more money in their pocket to buy out other breweries.  The problem with this, is the way they constantly push out smaller guys. The shelf space owned by these guys in stores is tremendous, and they have the ability to push the small guys right off the shelves. And they do so on a regular basis. You can learn more about this from the great documentary <a title="Beer Wars Movie" href="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/04/beerwarsmovie.com">Beer Wars</a>. I recommend everyone go see it on Netflix, Hulu, iTunes, the Beer Wars website, whatever. Just go watch it!<em id="__mceDel">
</em>

The latest trend in the seemingly never ending battle between big beer, and small beer (so to speak) is the label of 'Crafty Beer'. It seems the big guys finally realised their product isn't cutting it with consumers anymore, and more and more people are turning to 'craft beer' instead of the big boys. So, what are they to do? Well, create 'Crafty Beer' of course! They can slap a label on some beer saying it's '<a title="Youtube - Blue Moon commercial" href="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/04/watch?v=f5VbqU5Hrrw">Artfully Crafted</a>' and make it look like a craft beer, and call it good!
<p style="text-align: center;"><img class="aligncenter size-medium wp-image-260" alt="Artfully Crafted?" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/04/blue_moon-188x300.jpg" width="188" height="300" /></p>
Well, now the problem is, more and more people have fought back against this. Very recently, Yankee Stadium renamed their 'Craft Beer Station' due to the backlash. You may be asking yourself, what's wrong with a craft beer station, that would be great right? Well, you are right, however, <a title="NPR News - Beer Bust " href="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/04/beer-bust-yankees-rename-craft-beer-stand-at-stadium">there is a problem when said station is not serving craft beer, but, instead 'crafty beer' owned by Miller-Coors</a>. So, unfortunately, we now have to be more and more careful as to what we buy. We need to be more informed consumers as the waters are getting muddier and muddier.

And hey, I'm not saying you have to drink craft beer all.the.time. In fact, even Sam Calagione  (owner and brewer at Dogfish Head) has said he enjoys a 'big beer' at a baseball game once in a while. It would be great if the big brewers were to realize, if they were just make a better product, and care more about their consumers, most people would be fine with it. Even me, and anyone that knows me, knows I'm quite vocal against big beer, and bad beer in general.

So, next time you reach for a beer, do a little research before hand. Think about who and what you are supporting before cracking it open. Go to your local brewpub or brewery and support them directly. Don't be afraid to ask when you are at the bar "Have anything local?" And most of all, don't be afraid to try something new, you might surprise yourself!


