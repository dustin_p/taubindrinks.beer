---
title: "12 Hours Into Brewing"
date: 2013-04-13T20:39:07+00:00
categories:
  - Homebrewing
  - Post
tags:
  - brewing
  - post 
---
Woke up this morning, excited to see if there were any changes in the homebrew, and there were! Who knew brewing could be this exciting!

Good changes also! Things are happening  here buddy! The yeast is active and eating all the sugar, creating Co2, and alcohol. If you look at the shot attached, and compare it to yesterdays, there's quite a big difference. Following some advice I got on the <a title="Reddit Homebrewing" href="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/04/Homebrewing" target="_blank">homebrewing subreddit</a> I'll be picking up some tubing for a blowoff tube setup instead of the airlock I currently have. I don't want this bad boy to make a huge mess all over the place, effectively ending my brewing hobby as my wife would kill me I'm sure

<!--more-->

Now for the photo:

[caption id=""width="301"]<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/04/photostream"><img class=" " alt="Day 1 of brewing" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/04/8646639170_cf79179a2f_c.jpg" width="301" height="800" /></a> Day 1

I did realize this morning, there was one thing I forgot to do. I forgot to get the OG of the brew, and make note of it. I'll have to remember to do that next batch. I was just too excited for this one! I'll also be getting a nice molskine or similar to keep up with my brewing in. I'll also be keeping up with my brewing on here, so if you are interested, please keep checking back. You can also follow my posts by subscribing via email so you get notified every time there's a new post.

&nbsp;

Here's to a new hobby, and a tasty one at that! With a bit of luck, this will be one of the tastier beers I've ever had. We'll find out in a month or so. For now, stay safe and drink good beer! Cheers!

