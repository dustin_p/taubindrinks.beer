---
title: Grand Cru moved to secondary
author: Dustin Plunkett
type: post
date: 2014-05-28T23:10:40+00:00
categories:
  - Beer
  - Blog
  - Homebrewing
tags:
  - Blog
  - brewing
  - Craft beer
  - Grand Cru
---

My Grand Cru has been moved to secondary! In another 1-2 weeks, I'll be bottling my BrewFerm Grand Cru, then the final portion of the waiting game begins. Once the beer is bottled, it will need to sit for a few weeks before it's ready for show time. I can say however, it is tasting great so far, with what I've had during hydrometer readings.

&nbsp;

<!--more-->

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/05/secondary.jpg"><img class="size-full wp-image-747" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/05/secondary.jpg" alt="Secondary Fermentation" width="474" height="316" data-mce-width="474" data-mce-height="316" /></a> Secondary fermentation

I went to my local homebrew shop, and picked up a second fermentation bucket, in order to move this beer to secondary. I'll also be starting a new beer soon, while this one matures. My next Grand Cru update will be bottling day! I'm pretty excited for this beer, and can't wait to see how it turns out. If the tastings so far are any indication, this will be my best beer yet! I can't wait to try it.

In other news, I have rejoined the <a title="Beer Cellar NZ Beer Club" href="http://www.beercellar.co.nz/Beer-Club/" target="_blank">Beer Cellar NZ's beer club</a>. Each month they send you approximately 4 liters of beer, from all around the world. I had been a member for many months a while ago, and had stopped simply because I wasn't entirely satisfied with the beer selection for a few months in a row.  Since then, I have been keeping an eye on the club, and what's shipped, and determined the selection has gotten a lot better, so I rejoined!

My first new shipment arrived, and I was very pleasantly surprised, to find they include a sheet with all of the beers, for tasting notes! This, for me, is a great addition to the beer of the month club, as it will allow me to keep track of not only the beers I've had from them, but also my own notes on them.

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/05/bclub.jpg"><img class="size-full wp-image-748 aligncenter" title="" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/05/bclub.jpg" alt="beerclub" width="474" height="316" data-mce-width="474" data-mce-height="316" /></a> Beer Club Tasting Notes

Due to this addition, I'll be filing out notes for each of the beers every month, and giving my thoughts on them, including images, and other important facts. I plan on doing this once a month for each club shipment, probably around the time I get each shipment. So this months club notes should be up with images, in about 4 weeks, when next months beer arrives.

This months box included the following:

Weezledog DickleDoi Imperial Ruby Ale - Weezledog Brewing Company, New Zealand

Longbeard Blonde - Longbeard Ltd, New Zealand

Lagunitas Censored (Kronik) - Lagunitas Brewing Company, USA

Gulden Draak 9000 Quadruple - Brouwerij Van Steenberge, Belgium

Mongozo Banana - Brouwerij Huyghe, Belgium

Warsteiner Premium Pilsener - Warsteiner Brauerei, Germany

&nbsp;

Photos and notes from each of these fine beers will be posted next month! Until then, stay safe and drink good beer!

&nbsp;


