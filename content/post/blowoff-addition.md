---
title: Blowoff Addition
author: Dustin Plunkett
type: post
date: 2013-04-13T23:36:26+00:00
---

After reading quite a few suggestions and horror stories from Reddit. I've added a blowoff system to the brewing setup. Hopefully this will keep there from being any catastrophic failures.

<!--more-->

&nbsp;

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/04/8647107748"><img alt="Addition of the blowoff system" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/04/8647107748_21a576c615_c.jpg" width="534" height="800" /></a> Blowoff
