---
title: A view of my next homebrew – Brewferm Grand Cru
author: Dustin Plunkett
type: post
date: 2014-04-30T03:40:12+00:00
categories:
  - Beer
  - Homebrewing
tags:
  - Blog
  - Brewferm
  - brewing
---

It's been a while since I <a title="Mangrove Jack’s Night Watchman Porter" href="https://taubindrinks.beer/night-watchman-porter/" target="_blank">brewed my last homebrew</a>, and I figured I'd give everyone a view of my next homebrew - <a title="Brewferm" href="/images/2014/04/index.htm" target="_blank">Brewferm</a> Grand Cru as well as an update on my last two beers.

I'm pretty excited about this, possibly more so than my last two. This time around, I kind of know what to expect. I've been here before, standing at the starting line of my next homebrew. I'm familiar with the feelings of excitement that go into making your own beer. I have an expectation going into this that I didn't quite have with the last two. I also have a tiny bit of experience, not much, but every little bit helps.

<!--more-->

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/04/logo_brewferm_big-1.png" target="_blank"><img class="aligncenter" title="Brewferm Logo" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/04/logo_brewferm_big-1.png" alt="Brewferm Logo" width="300" height="71" data-mce-width="300" data-mce-height="71" /></a>

First though, a little bit about Brewferm. They are a Belgian homebrew manufacturer that makes brewing kits, malt extract kits, full malt kits, and basically anything else needed to brew your own beer. They specialize in Belgian styles (Dubbel, Tripel, Quad, Grand Cru, etc...) Some variants of these styles most will be familiar with would be <a title="Chimay Trappist Ale" href="/images/2014/04/en.html?IDC=27" target="_blank">Chimay</a>, <a title="Duvel Belgian Blonde" href="/images/2014/04/en" target="_blank">Duvel</a>, <a title="St Bernardus " href="/images/2014/04/?l=en" target="_blank">St. Bernardus</a> and many others.

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/04/chimay.jpg" target="_blank"><img class="aligncenter" title="Chimay White" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/04/chimay.jpg" alt="Chimay White - Belgian Tripel" width="300" height="225" data-mce-width="300" data-mce-height="225" />

The Belgian family of beers are some of my favorite, and usually my go-to beers when introducing someone to craft beers for the first time. One of the reasons for this, is that Belgian ales tend to be a lot more malty than hoppy. Most people who have never had a real beer are not used to, and generally turned off by, really hoppy beers. Belgian styles of beer tend to be extremely approachable by both new craft beer drinkers, and veterans alike (in fact, St Bernardus 12 which is a Quad, is one of the highest rated beers on <a title="Beer Advocate St Bernardus Quad" href="/images/2014/04/1708" target="_blank">Beer Advocate</a> and <a title="Rate Beer St Bernardus 12" href="/images/2014/04/2530" target="_blank">Rate Beer</a>).

Back to my next homebrew, when I first entered <a title="Hauraki Homebrew" href="/images/2014/04/haurakihomebrew.co.nz" target="_blank">Hauraki Homebrew </a>and spoke with the great people there about getting into brewing, they actually had recommended Brewferm. I was standing in their shop debating between the Brewferm Tripel (which will be my next beer once I've brewed the Grand Cru), and the <a title="Mangrove Jack's" href="/images/2014/04/mangrovejacks.com" target="_blank">Mangrove Jack's </a>bitter. I ended up going with the bitter at the time, solely because I had just enjoyed a really great bitter prior to going in. I was also a little intimidated by the non-english container.

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/04/grand_cru.jpg" target="_blank"><img class="aligncenter" title="Brewferm Grand Cru" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/04/grand_cru.jpg"  alt="Brewferm Grand Cru" width="474" height="316" data-mce-width="474" data-mce-height="316" /></a>

Now that I've had a couple of homebrews under my belt so to speak, I'm ready for the challenge. There are a few things that are a little different about the Brewferm range, one of which is the mandatory racking to secondary. What this means, is transferring the beer from its primary fermentation container, to a second container to finish its fermentation process. This will be my first time doing a secondary fermentation before bottling.

The other thing I've read about this beer, is the amount of time it takes to age to perfection. Basically it's a 6-2-26 schedule. 6 weeks in primary fermentation, 2 weeks in secondary, 26 weeks in the bottle before drinking (yes, that's 6 months). But, from what I've read, it's very rewarding. I'm very excited, and will of course, be documenting the entire process here.  If it turns out as good as I'm hoping, I think I might do these more often, and even experiment with them a bit. Either way, it should be really good!

I look forward to sharing my next adventure with you all, and hopefully do a bit more sharing a long the way. I know I've been lax, and I promise I will try to do better. As for my Cannon Fodder and Pukeko Porter, they have both gotten a lot better with age. In fact, I'll be having a Pukeko Porter tonight with my dinner! Until my first Brewferm update, Cheers!

