---
title: Dogfish Head Sixty-One – Taubin Drinks Tuesday
author: Dustin Plunkett
type: post
date: 2014-11-04T00:37:13+00:00
categories:
  - Blog
  - Taubin Drinks Episodes
tags:
  - Beer
  - Blog
  - Craft beer
  - Dogfish Head
  - Taubin Drinks Tuesday
---
Dogfish Head Sixty-One is a continually hopped IPA created with Syrah (also known as Shiraz)  grape must. This may be my new go-to for non beer drinkers.  This beer has an amazing color and flavor. Find out why Dogfish Head Sixty-One may be one of my favorite beers of all time.
    
<!--more-->

{{< youtube GF3knCzUTBU >}}


