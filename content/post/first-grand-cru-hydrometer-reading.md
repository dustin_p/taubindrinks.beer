---
title: First Grand Cru Hydrometer Reading
author: Dustin Plunkett
type: post
date: 2014-05-11T23:04:37+00:00
categories:
  - Beer
  - Beer Porn
  - Blog
  - Homebrewing
tags:
  - Blog
  - Brewferm
  - brewing
  - Craft beer
  - Grand Cru
  - Homebrew
---

It's been 10 days since brew day, so it's time to take the first Grand Cru hydrometer reading and see how it's fermenting. I was actually going to do this Saturday, however we had a little incident while taking everything down to the garage to take the reading. As my wife and I were juggling all of the things needed to take the reading, we accidentally dropped my hydrometer. It made a sickening crack sound. No huge deal, hydrometers are cheap, but it did postpone the reading. So I ran up to my local homebrew store, <a title="Hauraki Homebrew" href="http://haurakihomebrew.co.nz/">Hauraki Homebrew </a>and picked up a new one.

<!--more-->

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/05/hydrometer.jpg" target="_blank"><img class="aligncenter" title="" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/05/_d_improd_/hydrometer" alt="" width="300" height="450" data-mce-width="300" data-mce-height="450" /></a> Sad hydrometer

Wit that done, it was time to take the first Grand Cru hydrometer reading. I didn't know what to expect, as the fermenter is in the garage, and I haven't been able to baby-sit it, which is honestly probably for the best. So, I loaded everything up again, and headed down stairs to check things out.

I grabbed my vessel and poured some of the beer into it straight from the fermenting bucket, and boy was I happy with what I saw, and smelled. The beer has a beautiful gold color, and a very pleasant smell. I had heard these kits can be very aggressive, but was still a bit surprised at the amount of head given even while pouring into the testing tube. I didn't get a photo, as I was too excited but I wish I had.

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/05/hydrometer-3.jpg" target="_blank"><img class="aligncenter" title="" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/05/_d_improd_/hydrometer-3" alt="" width="300" height="200" data-mce-width="300" data-mce-height="200" /></a> Beautiful color

With the beer poured into the testing vessel, it was time to take a hydrometer reading. For those that aren't familiar, a hydrometer tests the specific gravity of liquids. In basic terms, it tests the amount of sugar to liquid when used for testing beer. This allows you to find out when the beer is ready to bottle (or in this case go into secondary).The target final gravity for this beer is 1.010. We'll take a first reading, then wait at least 24 hours (in this case it will be longer, but I'll get into that in a minute), and test again. If both tests are the same, it's ready to go into secondary.

So, let's take a look:

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/05/hydrometer-6.jpg" target="_blank"><img class="aligncenter" title="" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/05/_d_improd_/hydrometer-6" alt="" width="300" height="450" data-mce-width="300" data-mce-height="450" /></a> Hydrometer Reading

It's a bit hard to see from this angle, however you can see a little bit of the head that was left after I siphoned a bit of it off. It is quite difficult to try to take a reading through the head of the beer. Let's see if we can get a better look at the reading.

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/05/hydrometer-5.jpg" target="_blank"><img class="aligncenter" title="" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/05/_d_improd_/hydrometer-5" alt="" width="300" height="450" data-mce-width="300" data-mce-height="450" /></a> Better look

It looks to be about 1.016 or so. Still a bit high, so we'll let it sit for a few more days and test again to see if it has gone down at all, or if this is where it has settled.

I can definitively say, I am extremely happy with this kit so far. The color, scent and taste are all in line with what I was hoping for. This kit actually tastes a bit better than my last couple did. I'm not sure if it was the sanitizer, the kit, the fermenter, or something else. It may be a small kit, but I'll definitely be brewing another from these guys in the future. I can't wait to see how it finally turns out in the end, and I'll definitely keep you guys updated on the progress. And if anyone has any suggestions for a name for this beer, please let me know. Until then, drink good beer!

