---
title: Drunken Pukeko
author: Dustin Plunkett
type: post
date: 2014-06-16T04:58:17+00:00
categories:
  - Beer
  - Beer Porn
  - Blog
  - Homebrewing
tags:
  - Beer
  - Blog
  - Drunken Pukeko
  - Epic Brewing
  - Homebrew
  - Mongonzo
---

The other day I finally found the time to bottle my Brewferm kit. Over all, I ended up with 17 500 ml bottles, which for the size of the kit, is pretty close to spot on. That equates to 8.5 liters or 2.24 gallons of beer. Now the waiting game begins. The beer will sit in the bottles for a few weeks in my garage, and then it will finally be time to have a taste! I'm really excited about this beer, as during brewing and taking gravity readings, I've had small tastes of the cast off, and it's been great.

<!--more-->

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/06/IMG_8230.jpg"><img class="size-full wp-image-761" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/06/IMG_8230.jpg" alt="Bottled Grand Cru" width="683" height="1024" /></a> Bottled Grand Cru



I tried a few new things this time around, which have made things easier overall. Firstly, I have tried a new sterilizer. I have yet to try Star San, which everyone raves about (next time, I promise!), but I used a liquid this time which was much easier than the powder I used before.  I also picked up a second fermentation bucket, which has served as a great bottling bucket, as well as a place to perform a secondary. It made my life SO MUCH easier than the prior two.

There has also been a somewhat major change to the name I'll be brewing under. From here on, the name will be <a title="Drunken Pukeko Brewing" href="https://drunkenpukeko.com" target="_blank">Drunken Pukeko Brewing</a>. I'm going to be getting serious about brewing, and the first step for me, is a serious name. I want to step up my game, and get into all grain brewing.  While over at a good friends house the other day, he threw out the name Drunken Pukeko Brewing while talking about a name for my most recent beer, and it's stuck.

Speaking of awesome friends, he also brought me some amazing Stone Brewing beer from San Diego! Anyone that knows me at all, knows Stone is one of my favorite breweries in the world. They are the first brewery I visited and I fell in love immediately. He hand delivered a Stone <a title="Quadrotricale" href="http://www.stochasticity.com/beers/quadrotriticale" target="_blank">Stochasticity Quadrotriticale</a> and a Stone <a title="SPRÖCKETBIER" href="http://www.stonebrew.com/spotlight/sprocketbier/" target="_blank">SPRÖCKETBIER</a> (I know best friend EVER)! You can definitely look forward to reviews and images of both of these fine beers in the coming period, as well as the beers in my beer of the month club.

<a href="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/06/IMG_8234-2.jpg"><img class="size-full wp-image-766" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2014/06/IMG_8234-2.jpg" alt="Stone and Epic Brewing" width="683" height="1024" /></a> Stone and Epic Brewing

I also received a 4 pack of the not-yet-released Epic Apocalypse IPA via the courier this morning. So, friend who brought me some amazing Stone beers, one of those is yours next time I see you. In addition, I've tried each of the beers that came in my beer of the month club, and I can honestly say, almost all of them were excellent! In fact, you can see a teaser of one of them in the title image of this post.

I will definitely bring updates once the latest beer has been aged a few weeks, and come up with a name for the beer itself. It's a Belgian Strong ale, so if anyone has suggestions, please let me know! Until then, stay safe and drink good beer!

