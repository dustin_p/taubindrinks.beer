---
title: How to ensure I will never buy your beer again
author: Dustin Plunkett
type: post
date: 2013-07-05T05:41:51+00:00
categories:
  - Beer
  - Blog
tags:
  - big breweries
  - Craft beer
---

I'm a huge craft beer advocate. I preach craft beer to anyone that will listen, and even some that won't. Anyone who has spent more than 20 minutes in my presence, has probably been preached to in one way or another. Whether it be me talking about the evil of big beer (I don't mean high abv beer), or talking about the latest beer that a local brewery has come out with. I preach on the pulpit of craft beer every chance I get. To the extent that I have no idea how my poor wife deals with me.

<!--more-->

There are few things a brewery can do that will make sure I will never buy your product. Even more than just being a big brewery that couldn't care less about anything other than their bottom line. One of those things, is to mislead your customer base. I don't care if you are a big brewer, small brewer, big company, small company or whatever. You should NEVER mislead your customer base. It's one of the biggest sins that will lose me as a customer forever.

Why do I bring this up? Especially on a beer blog? It's because of an email I received a short bit ago. It came from a New Zealand brewery that I have had a few times, and that makes somewhat decent beer. That's not to say it's bad, it's just not great. That brewery is Mac's.

<img class="size-medium wp-image-347 " title="Mac's beer logo" alt="Mac's beer logo" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/07/macs.jpg" width="300" height="300" /> Mac's logo

&nbsp;

Now, what could be so bad about this if their beer isn't horrible? Well the title of the email is this: "Crafty Prizes and Free Mac’s Vouchers"

What's so wrong with that you ask? It's the first word. "Crafty" I can't stand it. Either you are a craft brewery, or you are not. EIther it is craft beer or it is not. Stop trying to deceive people into buying your product by using made up word that people will think means something it doesn't. Using phrases like "Crafty" "Artfully Crafted" and the like to sell your product to unsuspecting people is unethical and wrong. It's also a way to ensure I will never buy your beer again.

There is a simple, great way to get more customers that does not involve deceit. It's called make good beer. I know that's a crazy concept, however, it will work, I promise! Stop trying to convince people you are something you're not, and make good product. It's like the line in that movie "If you build it, they will come". Well, if you make great beer, they will come, and as many great craft breweries have proved, they will come in droves.

Also, creating "new" breweries under your label. There is a prime example here of both things in New Zealand, done by <a title="Lion Brewery Brands" href="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/07/new-zealand-beer">Lion</a>. They tried this with their "Crafty Beggers" line of beer. Needless to say, it <a title="NZ Herald - Lion's craft beer ploy has rival brewers frothing" href="/images/2013/07/article.cfm?c_id=3&amp;objectid=10868971">didn't go over well at all</a> with craft brewers or craft beer drinkers here. There were even some markets that would not carry the brand. And I can't blame them for that.

<img class=" wp-image-359  " alt="Crafty Beggers" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/07/crafty-beggers.jpg" width="550" height="430" /> Crafty Beggers

So please brewers and breweries. Don't be that guy. You have a customer base that, for reasons beyond my understanding, are already very loyal. Do not try to be something you're not. And stop trying to deceive people into thinking you're something you're not. It will be seen through very easily, and you will ensure that I will never buy your beer again. And I won't be alone in that.

