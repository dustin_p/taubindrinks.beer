---
title: Video – First all grain beer with the Grainfather
author: Dustin Plunkett
type: post
date: 2014-09-19T07:19:37+00:00
categories:
  - Blog
tags:
  - Blog
---

I completed my <a title="First brew – All grain brewing" href="https://taubindrinks.beer/first-brew-all-grain-brewing/" target="_blank">first all grain beer</a> using the <a title="The Grainfather" href="http://grainfather.co.nz" target="_blank">Grainfather</a>. Below is a video from the brew day.

<!--more-->

<iframe src="//www.youtube.com/embed/36k8CylANg4" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

