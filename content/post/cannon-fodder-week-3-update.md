---
title: Cannon Fodder week 3 update
author: Dustin Plunkett
type: post
date: 2013-05-20T07:04:11+00:00
categories:
  - Beer
  - Homebrewing
  - Taubin Drinks Episodes
tags:
  - brewing
  - Cannon Fodder
  - Homebrew
---

Cannon fodder has been in the bottle for three weeks now, so I figured I would update you all on how it's ageing. I will admit, I tried the beer at the one and two week marks as well, and can report, it's getting better and better. The color is an amazing amber color and the taste, well the taste is wonderful.

<!--more-->

When I first tasted Cannon Fodder, I was a bit shocked at how decent it tasted to be honest. My hopes were not extremely high, as it was my first time and I expected quite a few mistakes, and not much flavour. I was honestly expecting a flat, ill tasting mess of horrible flavour. Boy was I shocked with what I found. I had a few small issues when brewing, including some over-priming of a few of the beers, but it seems to have come out just fine!

<a title="Cannon Fodder by taubinphoto, on Flickr" href="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/05/8727113411"><img title="Cannon Fodder" alt="Cannon Fodder" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/05/8727113411_4e5fb0627d.jpg" width="333" height="500" /></a> Cannon Fodder ESB

<span style="line-height: 1.714285714; font-size: 1rem;">The color, the smell, and the taste of Cannon Fodder was amazing. At the first test a week in the bottles, it was a bit sweet, but, it still tasted great. It had a little head, with a little head retention but it was drinkable! I've said since the beginning, that's all I was going for. The shock for me came in the fact that this wasn't just drinkable, it was GOOD! It tasted as good as some of the craft brewed bitters I've had. I had no idea such good beer could come from a such a simple kit. </span>

The first week when I tried the beer, it was a bit sweet, however, I did not get discouraged. Most home brewers say to leave it in the bottle for at least three weeks. This gives the yeast time to eat up all of that tasty sugar, and turn it into alcohol and carbonation. The second week I tried the beer, the sweetness had toned down a bit. Now that we are in the third week, wow what a difference. Almost all of the residual sweetness is gone, and it's tasting like a bitter should. I can say, without a doubt, Cannon Fodder is a success for me. Even though I'll be the only one ever having my first try, I am extremely pleased with it.

<iframe src="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/05/Oo4Ld_Ypjrk?rel=0" height="315" width="560" allowfullscreen="" frameborder="0"></iframe>
I cannot wait to make my next batch of beer. I'll be stepping up a little bit from just the pouch, and using a <a title="Mangrove Jacks Craft series box Night Watchman" href="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/05/night-watchman-brewery-box" target="_blank">Mangrove Jacks Craft Series Box</a> Night Watchman porter. It's a small step up, but, I have much higher expectations than I did when I went into my first beer. It will be a porter, which is one of my favorite styles of beer. It's always nice to crack open a nice malty porter during the cooler months, which we are coming up to here in New Zealand.

<img class="size-medium wp-image-332" alt="Mangrove Jacks Night Watchman" src="https://res.cloudinary.com/taubin/image/upload/q_auto/2013/05/NightWatchman-268x300.png" width="268" height="300" /> Night Watchman Porter

<span style="line-height: 1.714285714; font-size: 1rem;">What's after that? You'll have to stay tuned to find out. I can tell you this though, I won't be stopping brewing. I'm enjoying it way too much to stop. </span>


